<?php
/**
 * @file
 * Variable information for the ETSSEF module.
 */

/**
 * Implements hook_variable_info().
 */
function etssef_variable_info($options = array()) {
  $variables = array();

  $variables['etssef_tab_title'] = array(
    'title' => t('Separated shared elements forms tab title', array(), $options),
    '#description' => t("Used for the bundles of any translatable entity types with their 'Shared elements on translation forms' setting set to 'Only display on their own separate forms'.", array(), $options),
    'type' => 'string',
    'default' => t('Shared', array(), array('context' => 'Entity Translation: Separated shared elements form tab title') + $options),
    'localize' => TRUE,
    'required' => TRUE,
    'group' => 'regional_settings',
  );

  return $variables;
}
