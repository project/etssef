Entity Translation: Separated shared elements form (ETSSEF)

This module can relocate untranslatable fields and settings on content that all languages have in common, from their usual place amongst translatable fields on translation edit forms, onto their own individual page tab.

This is the opposite approach to [Entity Translation Unified Form (ETUF)](https://www.drupal.org/project/entity_translation_unified_form), which joins all translations together into a single form.

On the entity translation settings page (/admin/config/regional/entity_translation), set the 'Shared elements on translation forms' dropdown for each bundle to 'Only display on their own separate forms'.
